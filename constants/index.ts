import constants from "./constants";
import theme, {COLORS, FONTS, SIZES} from "./theme";
import images from "./images";
import icons from "./icons";
import {Data} from "./dummyData";

export {constants, theme, COLORS, SIZES, FONTS, images, icons, Data};
