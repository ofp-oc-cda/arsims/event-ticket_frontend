import icons from "./icons";
import {images} from "./index";

export const Data = [
    {
        id: 1,
        title: "Multi-lateral intermediate moratorium",
        description: "I'll back up the multi-byte XSS matrix, that should feed the SCSI application!",
        image: icons.OnBoarding1
    },
    {
        id: 2,
        title: "Automated radical data-warehouse",
        description: "Use the optical SAS system, then you can navigate the auxiliary alarm!",
        image: icons.OnBoarding2
    },
    {
        id: 3,
        title: "Inverse attitude-oriented system engine",
        description: "The ADP array is down, compress the online sensor so we can input the HTTP panel!",
        image: icons.OnBoarding3
    },
    {
        id: 4,
        title: "Monitored global data-warehouse",
        description: "We need to program the open-source IB interface!",
        image: icons.OnBoarding4
    }
]

export const Profile = [
    {
        id: 1,
        image: images.profile
    }
]
