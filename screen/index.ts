//Home Screen
import MainLayout from "./Home/MainLayout";

//All Setting Screens
import SettingsScreen from "./Settings/SettingsScreen";
import Terms from "./Settings/TermsScreen";
import Help from "./Settings/HelpScreen";
import ChangePassword from "./Settings/ChangePassword";

//Authentication Screens
import SignIn from "./Authentication/SignInScreen";
import Register from "./Authentication/RegisterScreen";

//Payment Screens
import Payment from "./Payment/PaymentScreen";
import PaymentAdd from "./Payment/PaymentAddScreen";

//OnBoarding Screen
import OnBoarding from "./OnBoarding/OnBoardingScreen";

//Profile Screens
import Profile from "./Profile/ProfileScreen";
import EditProfile from "./Profile/EditProfileScreen";

export {
    MainLayout,
    //Setting Screens
    SettingsScreen,
    Terms,
    Help,
    ChangePassword,
    //Authentication Screens
    SignIn,
    Register,
    //Payment Screens
    PaymentAdd,
    Payment,
    //OnBoarding Screen
    OnBoarding,
    //Profile Screens
    Profile,
    EditProfile
}
