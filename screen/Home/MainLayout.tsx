import React, {useContext} from 'react';
import {Image, ImageBackground, TextInput, TouchableOpacity, View} from 'react-native';
import Animated from 'react-native-reanimated';
import {COLORS, icons, images, SIZES} from '../../constants';
import themeContext from "../../theme/themeContext";
import Header from "../../components/Header";
import {useTranslation} from "react-i18next";
import BottomTabsMenu from "../../containers/BottomTabsMenu";

const MainLayout = ({drawerAnimatedStyle, navigation}: any) => {
    const theme = useContext(themeContext)
    const {t} = useTranslation();

    function renderHeader() {
        return (
            <Header
                color={theme.color}
                title={t("headers.home")}
                containerStyle={{
                    height: 50,
                    paddingHorizontal: SIZES.padding,
                    marginTop: 10,
                    alignItems: 'center'
                }}
                // title={selectedTab.toUpperCase()}
                leftComponent={
                    <TouchableOpacity
                        style={{
                            width: 40,
                            height: 40,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderWidth: 1,
                            borderColor: COLORS.gray2,
                            borderRadius: SIZES.radius
                        }}
                        onPress={() => navigation.openDrawer()}
                    >
                        <Image
                            source={icons.menu}
                        />
                    </TouchableOpacity>
                }
                rightComponent={
                    <TouchableOpacity
                        style={{
                            borderRadius: SIZES.radius,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <Image
                            source={images.ProfileImage}
                            style={{
                                width: 40,
                                height: 40,
                                borderRadius: SIZES.radius,
                                borderWidth: 1,
                                borderColor: COLORS.gray
                            }}
                        />
                    </TouchableOpacity>
                }
            />
        )
    }

    function renderTextInput() {
        return (
            <View style={{marginTop: SIZES.font, marginBottom: SIZES.base, paddingHorizontal: SIZES.font,}}>
                <View
                    style={{
                        width: "100%",
                        borderRadius: SIZES.font,
                        backgroundColor: COLORS.lightGray2,
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "space-between",
                        paddingHorizontal: SIZES.font,
                        paddingVertical: SIZES.small - 4,
                        borderWidth: 1, borderColor: COLORS.gray3
                    }}
                >
                    <View style={{flexDirection: "row", alignItems: "center", width: "90%"}}>
                        <Image
                            source={icons.search}
                            resizeMode="contain"
                            style={{width: 20, height: 15, marginRight: SIZES.base}}
                        />
                        <TextInput
                            placeholder="Find amazing events"
                            style={{flex: 1}}
                            // onChangeText={onSearch}
                        />
                    </View>
                    <Image
                        source={icons.filter}
                        resizeMode="contain"
                        style={{width: 20, height: 20, marginRight: SIZES.base, tintColor: COLORS.gray2}}
                    />
                </View>
            </View>
        )
    }

    return (
        <Animated.View
            style={{
                flex: 1,
                backgroundColor: theme.background,
                ...drawerAnimatedStyle,
            }}>
            <ImageBackground source={icons.Header} resizeMode={"cover"}>
                <View style={{
                    padding: SIZES.font,
                }}>
                    {renderTextInput()}
                </View>
            </ImageBackground>

            <View style={{flex: 1}}/>
            <BottomTabsMenu navigation={navigation}/>
        </Animated.View>
    );
};

export default MainLayout;
