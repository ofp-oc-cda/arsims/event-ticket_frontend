import React, {useState, useContext} from 'react';
import {Text, View, StyleSheet, Switch, ScrollView} from 'react-native';
import {EventRegister} from 'react-native-event-listeners';
import themeContext from "../../theme/themeContext";
import {COLORS, icons, SIZES} from "../../constants";
import Header from "../../components/Header";
import IconButton from "../../components/IconButton";
import {useTranslation} from "react-i18next";
import {password} from "../../assets";
import LineDivider from "../../components/LineDivider";
import IconLabelButton from "../../components/IconLabelButton";

const SettingsScreen = ({navigation}: any) => {
    const {t} = useTranslation();
    const [mode, setMode] = useState<boolean>(false)
    const theme = useContext(themeContext)

    const handleChange = () => {
        setMode(mode => {
            if (!mode) {
                setMode(true);
            } else {
                setMode(false);
            }
            return mode;
        });
    };

    function renderHeader() {
        return (
            <Header
                color={theme.color}
                title={t("headers.settings")}
                containerStyle={{
                    height: 50,
                    marginHorizontal: SIZES.padding,
                    marginTop: 20
                }}
                leftComponent={
                    <IconButton
                        icon={icons.back}
                        containerStyle={{
                            width: 40,
                            height: 40,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderWidth: 1,
                            borderRadius: SIZES.radius,
                            borderColor: COLORS.gray2,
                        }}
                        iconStyle={{
                            width: 20,
                            height: 20,
                            tintColor: COLORS.gray2
                        }}
                        onPress={() => navigation.goBack()}
                    />
                }
                rightComponent={
                    <View
                        style={{
                            width: 40
                        }}
                    />
                }
            />
        )
    }

    function renderSettings() {
        return (
            <View
                style={{
                    marginTop: SIZES.radius,
                    paddingHorizontal: SIZES.padding,
                    borderRadius: SIZES.radius,
                    backgroundColor: COLORS.lightGray2
                }}
            >
                <IconLabelButton
                    icon={icons.password}
                    label="Change Password"
                    containerStyle={styles.iconLabelContainerStyle}
                    iconStyle={styles.iconLabelIconStyle}
                    onPress={() => navigation.navigate("ChangePassword")}
                />

                <LineDivider
                    lineStyle={{
                        backgroundColor: COLORS.lightGray1
                    }}
                />

                <IconLabelButton
                    icon={icons.filter}
                    label="Preferences"
                    containerStyle={styles.iconLabelContainerStyle}
                    iconStyle={styles.iconLabelIconStyle}
                />

                <LineDivider
                    lineStyle={{
                        backgroundColor: COLORS.lightGray1
                    }}
                />

                <IconLabelButton
                    icon={icons.notification}
                    label="Notifications"
                    containerStyle={styles.iconLabelContainerStyle}
                    iconStyle={styles.iconLabelIconStyle}
                    onPress={() => navigation.navigate("NotificationSetting")}
                />

                <LineDivider
                    lineStyle={{
                        backgroundColor: COLORS.lightGray1
                    }}
                />
                <IconLabelButton
                    icon={icons.privacy}
                    label="Privacy Policy"
                    containerStyle={styles.iconLabelContainerStyle}
                    iconStyle={styles.iconLabelIconStyle}
                />

                <LineDivider
                    lineStyle={{
                        backgroundColor: COLORS.lightGray1
                    }}
                />

                <IconLabelButton
                    icon={icons.term}
                    label="Terms & Conditions"
                    containerStyle={styles.iconLabelContainerStyle}
                    iconStyle={styles.iconLabelIconStyle}
                />

                <LineDivider
                    lineStyle={{
                        backgroundColor: COLORS.lightGray1
                    }}
                />

                <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                    <IconLabelButton
                        icon={!mode ? icons.sunny : icons.moon}
                        label={mode ? "Dark mode" : "Light mode"}
                        containerStyle={styles.iconLabelContainerStyle}
                        iconStyle={styles.iconLabelIconStyle}
                    />

                    <Switch
                        trackColor={{false: COLORS.primary, true: COLORS.lightOrange}}
                        thumbColor={mode ? COLORS.primary : COLORS.lightOrange}
                        ios_backgroundColor={"#3e3e3e"}
                        value={mode}
                        onValueChange={() => {
                            EventRegister.emit("changeTheme", mode)
                            handleChange()
                        }}
                    />
                </View>

                <LineDivider
                    lineStyle={{
                        backgroundColor: COLORS.lightGray1
                    }}
                />

                <IconLabelButton
                    icon={icons.logout}
                    label="Logout"
                    containerStyle={styles.iconLabelContainerStyle}
                    iconStyle={styles.iconLabelIconStyle}
                />
            </View>
        )
    }

    return (
        <View style={{flex: 1,backgroundColor: theme?.background}}>
            {renderHeader()}
            <ScrollView contentContainerStyle={{
                paddingHorizontal: SIZES.padding,
            }}>
                {renderSettings()}
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
    },
    iconLabelContainerStyle: {
        height: 70,
        alignItems: 'center',
        paddingHorizontal: 0
    },
    iconLabelIconStyle: {
        marginRight: SIZES.radius,
        tintColor: COLORS.primary
    }
})

export default SettingsScreen;
