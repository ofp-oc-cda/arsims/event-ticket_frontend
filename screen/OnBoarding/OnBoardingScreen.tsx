import React, {useRef} from "react";
import {
    Animated,
    Dimensions,
    Image,
    ImageSourcePropType,
    ListRenderItemInfo,
    StatusBar,
    StyleSheet,
    Text,
    View
} from 'react-native';
import {COLORS, Data, FONTS, SIZES} from "../../constants";
import TextButton from "../../components/TextButton";

const {width, height} = Dimensions.get('screen');

const bgs = ['#A5BBFF', '#DDBEFE', '#FF63ED', '#B98EFF'];
const bg = '#724ab2';

interface OnBoardingProps {
    id: number,
    title: string,
    description: string,
    image: ImageSourcePropType
}

const Indicator = ({scrollX}: any) => {
    return (
        <View style={{position: "absolute", bottom: 20, flexDirection: "row"}}>
            {Data.map((_, i) => {
                const inputRange = [(i - 1) * width, i * width, (i + 1) * width]
                const scale = scrollX.interpolate({
                    inputRange,
                    outputRange: [0.8, 1.4, 0.8],
                    extrapolate: "clamp"
                });
                const opacity = scrollX.interpolate({
                    inputRange,
                    outputRange: [0.6, 0.9, 0.6],
                    extrapolate: "clamp"
                })
                return <Animated.View
                    key={`indicator-${i}`}
                    style={{
                        height: 10,
                        width: 10,
                        borderRadius: 10,
                        opacity,
                        backgroundColor: COLORS.white,
                        margin: SIZES.base,
                        transform: [{scale}]
                    }}
                />
            })}
        </View>
    )
}

const Backdrop = ({scrollX}: any) => {
    const backgroundColor = scrollX.interpolate({
        inputRange: bgs.map((_, i) => i * width),
        outputRange: bgs.map(bg => bg)
    })
    return <Animated.View
        style={[
            StyleSheet.absoluteFillObject,
            {backgroundColor}
        ]}
    />
}

const Square = ({scrollX}: any) => {
    const YOLO = Animated.modulo(Animated.divide(
        Animated.modulo(scrollX, width),
        new Animated.Value(width)
    ), 1);

    const rotate = YOLO.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: ["35deg", "0deg", "35deg"]
    })

    const translateX = YOLO.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [0, -height, 0]
    })
    console.log("Rotate", rotate)

    return <Animated.View
        style={{
            width: height,
            height,
            backgroundColor: COLORS.white,
            borderRadius: 86,
            position: "absolute",
            top: -height * 0.65,
            left: -height * 0.35,
            transform: [
                {
                    rotate: "35deg",
                },
                // {
                //  translateX,
                // }
            ]
        }}
    />
}

const RenderButton = ({navigation}: any) => {
    const backgroundColor = bg
    return (
        <View style={{
            flexDirection: "row",
            justifyContent: "space-around",
            marginTop: -100,
            marginBottom: 70
        }}>
            <TextButton
                buttonContainerStyle={{
                    ...styles.textButtonContainer,
                    backgroundColor,
                    paddingHorizontal: SIZES.padding,
                    marginRight: SIZES.padding
                }}
                label={"Sign in"}
                labelStyle={{...FONTS.body4}}
                onPress={() => navigation.navigate("Home")}
            />

            <TextButton
                buttonContainerStyle={{
                    ...styles.textButtonContainer,
                    backgroundColor,
                    paddingHorizontal: SIZES.padding
                }}
                label={"Create Account"}
                labelStyle={{...FONTS.body4}}
            />
        </View>
    )
}

const OnBoarding = ({navigation}: any) => {
    const scrollX = useRef(new Animated.Value(0)).current
    return (
        <View style={styles.container}>
            <StatusBar hidden/>
            <Backdrop scrollX={scrollX}/>
            <Square/>
            <Animated.FlatList
                data={Data}
                horizontal
                scrollEventThrottle={32}
                onScroll={Animated.event(
                    [{nativeEvent: {contentOffset: {x: scrollX}}}],
                    {useNativeDriver: false}
                )}
                showsHorizontalScrollIndicator={false}
                pagingEnabled
                contentContainerStyle={{paddingBottom: 100}}
                keyExtractor={(item: any) => item.id}
                renderItem={({item}: ListRenderItemInfo<OnBoardingProps>) => {
                    return (
                        <View style={{width, alignItems: "center", padding: 20}}>
                            <View style={{flex: 0.7, justifyContent: "center", marginTop: 50}}>
                                <Image
                                    source={item.image}
                                    style={{
                                        width: width / 2,
                                        height: width / 2,
                                        resizeMode: "contain"
                                    }}
                                />
                            </View>
                            <View style={{flex: 0.3, marginTop: 20}}>
                                <Text style={{
                                    color: COLORS.white,
                                    fontWeight: "800",
                                    fontSize: 28,
                                    paddingVertical: 10
                                }}>{item.title}</Text>
                                <Text style={{color: COLORS.white, fontWeight: "300"}}>{item.description}</Text>
                            </View>
                        </View>
                    )
                }}
            />
            <RenderButton scrollX={scrollX} navigation={navigation}/>
            <Indicator scrollX={scrollX}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textButtonContainer: {
        height: 50,
        borderRadius: SIZES.radius,
    }
});

export default OnBoarding;
