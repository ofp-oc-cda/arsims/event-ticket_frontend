import React, {useContext} from "react"
import Header from "../components/Header";
import {COLORS, images, SIZES} from "../constants";
import {Image, TouchableOpacity, View} from "react-native";
import themeContext from "../theme/themeContext";
import {useTranslation} from "react-i18next";


const HeaderContainer = ({onPress, title, icon}: any) => {
    const theme = useContext(themeContext)
    const {t} = useTranslation();

    function renderRight() {
        return (
            <TouchableOpacity
                style={{
                    borderRadius: SIZES.radius,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <Image
                    source={images.ProfileImage}
                    style={{
                        width: 40,
                        height: 40,
                        borderRadius: SIZES.radius,
                        borderWidth: 1,
                        borderColor: COLORS.gray
                    }}
                />
            </TouchableOpacity>
        )
    }
    return (
        <Header
            color={theme.color}
            title={title}
            containerStyle={{
                height: 50,
                paddingHorizontal: SIZES.padding,
                marginTop: 20,
                alignItems: 'center'
            }}
            // title={selectedTab.toUpperCase()}
            leftComponent={
                <TouchableOpacity
                    style={{
                        width: 40,
                        height: 40,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                        borderColor: COLORS.gray2,
                        borderRadius: SIZES.radius
                    }}
                    onPress={onPress}
                >
                    <Image
                        source={icon}
                    />
                </TouchableOpacity>
            }
            rightComponent={
                <View
                    style={{
                        width: 40
                    }}
                />
                // renderRight()
            }
        />
    )
}

export default HeaderContainer;
