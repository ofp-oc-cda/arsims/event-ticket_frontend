import React, {ComponentProps} from "react";
import {Alert, StyleSheet, Text, View} from "react-native";
import Feather from 'react-native-vector-icons/Feather';
import {TouchableWithoutFeedback} from "react-native-gesture-handler";

interface RowPops {
    label: string,
    icon: ComponentProps<typeof Feather>["name"]
}

const Row = ({label, icon}: RowPops) => {
    return (
        <TouchableWithoutFeedback
            onPress={() => Alert.alert(label)}
        >
            <View style={styles.container}>
                <Text style={styles.label}>{label}</Text>
                <Feather name={icon} color={"white"} size={24}/>
            </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
    },
    label: {
        color: "white",
        textAlign: "center",
        fontSize: 18,
        fontFamily: "GothamRounded-Medium",
        marginRight: 8,
    },
});


export default Row;
