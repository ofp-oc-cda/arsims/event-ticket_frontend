import React from "react";
import {Pressable, StyleSheet} from "react-native";
import Animated, {useAnimatedProps, useAnimatedStyle, withTiming,} from "react-native-reanimated";
import {COLORS} from "../../constants";

interface BackdropProps {
    open: Animated.SharedValue<number>;
}

const Backdrop = ({open}: BackdropProps) => {
    const animatedProps = useAnimatedProps(() => ({
        pointerEvents: open.value < 1 ? ("none" as const) : ("box-none" as const)
    }));
    const style = useAnimatedStyle(() => ({
        backgroundColor: COLORS.gray,
        opacity: 0.6 * open.value
    }));

    return (
        <Animated.View
            style={[StyleSheet.absoluteFillObject, style]}
            animatedProps={animatedProps}
        >
            <Pressable
                style={StyleSheet.absoluteFillObject}
                onPress={() => (open.value = withTiming(0))}
            />
        </Animated.View>
    )
}

export default Backdrop;
