import React from "react";
import { Dimensions, StyleSheet, View } from "react-native";
import Feather from 'react-native-vector-icons/Feather';
import { useSafeAreaInsets } from "react-native-safe-area-context";

const { width } = Dimensions.get("window");
export const SIZE = width / 5;

const StaticTabBar = () => {
    const insets = useSafeAreaInsets();
    return (
        <View style={[styles.container, { paddingBottom: 16 + insets.bottom }]}>
            <Feather name="copy" color="#B9B9C7" size={24} />
            <Feather name="activity" color="#B9B9C7" size={24} />
            <Feather name="x" color="#B9B9C7" size={24} />
            <Feather name="edit-3" color="#B9B9C7" size={24} />
            <Feather name="user" color="#B9B9C7" size={24} />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width,
        backgroundColor: "white",
        flexDirection: "row",
        justifyContent: "space-evenly",
        borderTopRightRadius: 32,
        borderTopLeftRadius: 32,
        paddingTop: 32,
    },
});

export default StaticTabBar;
