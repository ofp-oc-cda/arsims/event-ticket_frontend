import React from "react"
import {Image, Text, TouchableOpacity, View} from "react-native";
import {COLORS, FONTS, SIZES} from "../constants";
import {useAppSelector} from "../hooks/useAppSelector";

const TabButton = ({label, icon, onPress}: any) => {
    const active = useAppSelector(state => state.tabs.active)

    return (
        <TouchableOpacity
            onPress={onPress}
            activeOpacity={0.5}
        >
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                    paddingHorizontal: 5
                }}
            >
                <View
                    style={{
                        flexDirection: 'row',
                        height: 50,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 30,
                    }}
                >
                    <Image
                        source={icon}
                        style={{
                            width: 20,
                            height: 20,
                            tintColor: active ? COLORS.white : COLORS.white
                        }}
                    />

                    {active &&
                        <Text
                            numberOfLines={1}
                            style={{
                                marginLeft: SIZES.base,
                                color: COLORS.white,
                                ...FONTS.h3
                            }}
                        >
                            {label}
                        </Text>
                    }
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default TabButton;
