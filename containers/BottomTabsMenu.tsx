import {COLORS, constants, icons, SIZES} from "../constants";
import {View} from "react-native";
import TabButton from "./TabButton";
import React from "react";
import {useAppDispatch} from "../hooks/useAppDispatch";
import {setActive} from "../redux/reducers/tabs";
import {useAppSelector} from "../hooks/useAppSelector";

const BottomTabsMenu = ({navigation}: any) => {
    const dispatch = useAppDispatch();
    const selectedTab = useAppSelector(state => state.tabs.selectedTab)

    return (
        <View
            style={{
                height: 95,
                alignItems: 'flex-start',
                borderBottomRightRadius: 15
            }}
        >
            {/* Tabs */}
            <View
                style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: "space-between",
                    width: "100%",
                    paddingHorizontal: SIZES.radius,
                    paddingBottom: 10,
                    borderTopLeftRadius: 20,
                    borderTopRightRadius: 20,
                    backgroundColor: COLORS.primary
                }}
            >
                <TabButton
                    label={constants.screens.home}
                    icon={icons.home}
                    onPress={() => console.log("Home Screen")}
                />

                <TabButton
                    label={constants.screens.notification}
                    icon={icons.notification}
                    onPress={() => console.log("Notification Screen")}
                />

                <TabButton
                    label={constants.screens.cart}
                    icon={icons.cart}
                    onPress={() => console.log("Ticket Screen")}
                />

                <TabButton
                    label={constants.screens.favourite}
                    icon={icons.favourite}
                    onPress={() => console.log("Saved Screen")}
                />

                <TabButton
                    label={constants.screens.settings}
                    icon={icons.setting}
                    onPress={() => {
                        dispatch(setActive(true))
                        navigation.navigate("Settings")
                    }}
                />
            </View>
        </View>
    )
}

export default BottomTabsMenu;
