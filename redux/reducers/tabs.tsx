import {createSlice} from "@reduxjs/toolkit";

interface TabsProps {
    selectedTab: string
    active: boolean
}

const initialState: TabsProps = {
    selectedTab: "",
    active: false
}

const TabsSlice = createSlice({
    name: "tabs",
    initialState: initialState,
    reducers: {
        setSelectedTab(state, action) {
            state.selectedTab = action.payload
        },
        setActive(state, action) {
            state.active = action.payload
        }
    }
})

export default TabsSlice.reducer;

export const {
    setSelectedTab,
    setActive
} = TabsSlice.actions
