import { combineReducers } from "@reduxjs/toolkit";

import tabs from "./reducers/tabs"

export default combineReducers({
    tabs
})
