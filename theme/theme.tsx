import {Light} from "./Light";
import {Dark} from "./Dark";

const theme = {
    light: {
        color: Light.colors.black,
        background: Light.colors.secondary,
        statusBar: Light.colors.primary
    },
    dark: {
        color: Dark.colors.white,
        background: Dark.colors.background,
        statusBar: Dark.colors.background
    }
}

export default theme
