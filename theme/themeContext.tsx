import React, {createContext} from 'react';

type ThemeProps = {
    background: string
    color: string,
    statusBar?: string
}

const themeContext = createContext<ThemeProps>({
    background: "",
    color: "",
    statusBar: ""
})

export default themeContext;
