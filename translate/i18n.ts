import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import {LanguageDetector} from './LanguageDetector';

const fr = require('./fr.json');
const en = require('./en.json');
const de = require('./de.json');
const it = require('./it.json');

const resources = {
  en: {
    translation: en,
  },
  fr: {
    translation: fr,
  },
  de: {
    translation: de,
  },
  it: {
    translation: it,
  }
};

i18n
  .use(LanguageDetector)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    compatibilityJSON: 'v3',
    fallbackLng: 'en',
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
    resources,
  });

export default i18n;
