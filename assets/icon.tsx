import React from 'react';
import { StyleSheet } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import {COLORS} from '../constants';

const defaultStyle = StyleSheet.create({
    icon: {
        backgroundColor: 'transparent',
    },
});

export const FeatherIcon = (props: any) => (
    <Feather
        name={props.name}
        size={props.hasOwnProperty('size') ? props.size : 20}
        color={props.hasOwnProperty('color') ? props.color : COLORS.darkBlue}
        style={
            props.hasOwnProperty('style')
                ? [defaultStyle.icon, props.style]
                : defaultStyle.icon
        }
    />
);

export const FontAwesomeIcon = (props: any) => (
    <FontAwesome
        name={props.name}
        size={props.hasOwnProperty('size') ? props.size : 20}
        color={props.hasOwnProperty('color') ? props.color : COLORS.darkBlue}
        style={
            props.hasOwnProperty('style')
                ? [defaultStyle.icon, props.style]
                : defaultStyle.icon
        }
    />
)

export const SimpleLine = (props: any) => (
    <SimpleLineIcons
        name={props.name}
        size={props.hasOwnProperty('size') ? props.size : 20}
        color={props.hasOwnProperty('color') ? props.color : COLORS.darkBlue}
        style={
            props.hasOwnProperty('style')
                ? [defaultStyle.icon, props.style]
                : defaultStyle.icon
        }
    />
);
