import React, {useState} from 'react';
import {View} from 'react-native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {MainLayout} from '../screen';
import {COLORS} from '../constants';
import {CustomDrawerContent} from '../components';
import Animated from 'react-native-reanimated';
import {useAppSelector} from "../hooks/useAppSelector";

const Drawer = createDrawerNavigator();

const CustomDrawer = () => {
    const [progress, setProgress] = useState(new Animated.Value(0));
    const selectedTab = useAppSelector(state => state.tabs.selectedTab)

    const scale = Animated.interpolateNode(progress, {
        inputRange: [0, 1],
        outputRange: [1, 0.8],
    });

    const borderRadius = Animated.interpolateNode(progress, {
        inputRange: [0, 1],
        outputRange: [0, 26],
    });

    const animatedStyle = {borderRadius, transform: [{scale}]};

    return (
        <View
            style={{
                flex: 1,
                backgroundColor: COLORS.primary,
            }}>
            <Drawer.Navigator
                screenOptions={{
                    headerShown: false,
                    drawerType: 'slide',
                    overlayColor: 'transparent',
                    drawerStyle: {
                        flex: 1,
                        width: '65%',
                        paddingRight: 20,
                        backgroundColor: 'transparent',
                    },
                    sceneContainerStyle: {
                        backgroundColor: 'transparent',
                    },
                }}
                initialRouteName="MainLayout"
                drawerContent={(props: any) => {
                    setTimeout(() => {
                        setProgress(props.progress);
                    }, 0);

                    return (
                        <CustomDrawerContent
                            selectedTab={selectedTab}
                            navigation={props.navigation}
                        />
                    )
                }}>
                <Drawer.Screen name="MainLayout">
                    {(props: any) => <MainLayout {...props} selectedTab={selectedTab}
                                                 drawerAnimatedStyle={animatedStyle}/>}
                </Drawer.Screen>
            </Drawer.Navigator>
        </View>
    );
};

export default CustomDrawer;
