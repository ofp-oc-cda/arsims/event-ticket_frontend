import {CompositeScreenProps, NavigatorScreenParams} from "@react-navigation/native";
import {StackScreenProps} from "@react-navigation/stack";

export type AuthStackParamList = {
    Intro: undefined
    Home: undefined
}

export type AuthStackScreenProps<T extends keyof AuthStackParamList> = StackScreenProps<AuthStackParamList, T>

export type HomeStackParamList = {
    Home: undefined
    W3DSecure: {
        uri: string
    }
}

export type HomeStackScreenProps<T extends keyof HomeStackParamList> = CompositeScreenProps<StackScreenProps<HomeStackParamList, T>, RootStackScreenProps<keyof RootStackParamList>>

export type RootStackParamList = {
    Home: NavigatorScreenParams<HomeStackParamList>
    Settings: NavigatorScreenParams<SettingsStackParamList>
    OnBoarding: undefined
};

export type RootStackScreenProps<T extends keyof RootStackParamList> = StackScreenProps<RootStackParamList, T>

export type SettingsStackParamList = {
    Settings: undefined
    ChangePassword: undefined
    TermsConditions: undefined
    ContactUs: undefined
}

export type SettingStackScreenProps<T extends SettingsStackParamList> = CompositeScreenProps<StackScreenProps<SettingsStackParamList>, RootStackScreenProps<keyof RootStackParamList>>
