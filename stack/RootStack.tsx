import {createStackNavigator} from "@react-navigation/stack";
import React from "react";
import {RootStackParamList} from "./types";
import {MainLayout, OnBoarding} from "../screen";
import SettingsStack from "./SettingsStack";

const Root = createStackNavigator<RootStackParamList>();

const RootStack = () => {
    return (
        <Root.Navigator
            screenOptions={{
            headerShown: false,
        }}
            initialRouteName={"OnBoarding"}
        >
            <Root.Screen name={"OnBoarding"} component={OnBoarding} />
            <Root.Screen name={"Home"} component={MainLayout}/>
            <Root.Screen name={"Settings"} component={SettingsStack} />
        </Root.Navigator>
    )
}

export default RootStack;
