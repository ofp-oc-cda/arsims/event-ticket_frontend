import {createStackNavigator} from "@react-navigation/stack";
import React from "react";
import {SettingsStackParamList} from "./types";
import {SettingsScreen} from "../screen";

const Stack = createStackNavigator<SettingsStackParamList>();

const SettingsStack = () => {
    return (
        <Stack.Navigator
            initialRouteName={"Settings"}
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Screen name={"Settings"} component={SettingsScreen}/>
        </Stack.Navigator>
    )
}

export default SettingsStack;
