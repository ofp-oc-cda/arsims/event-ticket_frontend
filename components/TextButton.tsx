import React from "react";
import {ActivityIndicator, StyleSheet, Text, TouchableOpacity} from "react-native";

import {COLORS,FONTS} from "../constants";

const TextButton = ({
                        buttonContainerStyle,
                        disabled,
                        label,
                        labelStyle,
                        label2 = "",
                        label2Style,
                        onPress,
                        inProgress
                    }: any) => {
    return (
        <TouchableOpacity
            style={[styles.button ,{...buttonContainerStyle}]}
            disabled={disabled}
            onPress={onPress}
        >
            <Text style={{color: COLORS.white, ...FONTS.h3, ...labelStyle}}>
                {
                    inProgress ?
                        <ActivityIndicator
                            style={{
                                marginTop: 20,
                            }}
                            size="small"
                            color={"white"}
                        />: label
                }
            </Text>

            {label2 != 0 &&
                <Text style={[styles.text,{...label2Style}]}>
                    ${label2}
                </Text>
            }
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: COLORS.lightGray1,
        shadowColor: COLORS.lightGray1,
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,

        elevation: 10,
    },
    text: {
        flex: 1,
        textAlign: "right",
        color: COLORS.black,
        ...FONTS.h3,
    }
});

export default TextButton;
