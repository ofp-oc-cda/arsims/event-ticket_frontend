import React from 'react';
import {Text, View} from 'react-native';
import {FONTS} from "../constants"

const Header = ({containerStyle, title, titleStyle, leftComponent, rightComponent, color}: any) => {
    return (
        <View
            style={{
                height: 60,
                flexDirection: 'row',
                ...containerStyle
            }}
        >

            {
                leftComponent
            }
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text
                    style={{color: color, textTransform: "uppercase", ...FONTS.h3, ...titleStyle}}>{title}</Text>
            </View>
            {
                rightComponent
            }

        </View>
    )
}

export default Header
