import React from 'react';
import {DrawerContentScrollView} from '@react-navigation/drawer';
import {View, TouchableOpacity, Image, Text} from 'react-native';
import {COLORS, constants, dummyData, FONTS, icons, SIZES} from '../constants';
import CustomDrawerItem from './CustomDrawerItems';
import {useAppDispatch} from "../hooks/useAppDispatch";
import {setSelectedTab} from "../redux/reducers/tabs";

const CustomDrawerContent = ({navigation, selectedTab}: any) => {
    const dispatch = useAppDispatch();

    return (
        <DrawerContentScrollView
            scrollEnabled={true}
            contentContainerStyle={{flex: 1}}>
            <View style={{flex: 1, paddingHorizontal: SIZES.radius, padding: 15}}>
                <TouchableOpacity
                    style={{
                        flexDirection: 'row',
                        marginTop: SIZES.radius,
                        alignItems: 'center',
                    }}
                    onPress={() => console.log('Profile')}>
                    <Image
                        source={dummyData.myProfile?.profile_image}
                        style={{width: 50, height: 50, borderRadius: SIZES.radius}}
                    />
                    <View
                        style={{
                            marginLeft: SIZES.radius,
                        }}>
                        <Text style={{color: COLORS.white, ...FONTS.h3}}>
                            {dummyData.myProfile?.name}
                        </Text>
                        <Text style={{color: COLORS.white, ...FONTS.body4}}>
                            View your profile
                        </Text>
                    </View>
                </TouchableOpacity>
                {/* Drawer Item */}
                <View
                    style={{
                        flex: 1,
                        marginTop: SIZES.padding
                    }}
                >
                    <CustomDrawerItem
                        label={constants.screens.home}
                        icon={icons.home}
                        isFocused={selectedTab == constants.screens.home}
                        onPress={() => {
                            dispatch(setSelectedTab(constants.screens.home))
                            navigation.navigate("MainLayout")
                        }}
                    />
                    <CustomDrawerItem label={constants.screens.my_wallet} icon={icons.wallet}/>
                    <CustomDrawerItem
                        label={constants.screens.notification}
                        icon={icons.notification}
                        isFocused={selectedTab == constants.screens.notification}
                        onPress={() => {
                            dispatch(setSelectedTab(constants.screens.notification))
                            navigation.navigate("MainLayout")
                        }}
                    />
                    <CustomDrawerItem
                        label={constants.screens.favourite}
                        icon={icons.favourite}
                        isFocused={selectedTab == constants.screens.favourite}
                        onPress={() => {
                            dispatch(setSelectedTab(constants.screens.favourite))
                            navigation.navigate("MainLayout")
                        }}
                    />
                    {/* Line divider */}
                    <View style={{
                        height: 1,
                        marginVertical: SIZES.radius,
                        marginLeft: SIZES.radius,
                        backgroundColor: COLORS.lightGray1
                    }}/>
                    <CustomDrawerItem label={constants.screens.location} icon={icons.location}/>
                    <CustomDrawerItem
                        label={constants.screens.settings} icon={icons.setting}
                        onPress={() => {
                            navigation.closeDrawer()
                            navigation.navigate("Settings")
                        }}
                    />
                    <CustomDrawerItem label={constants.screens.friend} icon={icons.profile}/>
                    <CustomDrawerItem label={constants.screens.help} icon={icons.help}/>
                </View>
                <View
                    style={{
                        marginBottom: SIZES.padding
                    }}
                >
                    <CustomDrawerItem label={constants.screens.logout} icon={icons.logout}/>
                </View>
            </View>
        </DrawerContentScrollView>
    );
};

export default CustomDrawerContent;
