import React from "react";
import {Image, Text, TouchableOpacity, View} from "react-native";
import {COLORS, FONTS, icons, images, SIZES} from "../constants";

const CartItem = () => {
    return (
        <View style={{
            flexDirection: "row",
            height: 170,
            borderRadius: 20,
            borderWidth: 1,
            borderColor: COLORS.lightGray1,
            marginTop: 20,
        }}>

            <TouchableOpacity style={{
                position: "absolute",
                top: -15,
                right: 10,
                width: 30,
                height: 30,
                borderRadius: 50,
                backgroundColor: COLORS.primary,
                justifyContent: "center",
                alignItems: "center",
            }}>
                <Image source={icons.love} resizeMode={"cover"} style={{
                    width: 20,
                    height: 20,
                    tintColor: COLORS.white,
                }}/>
            </TouchableOpacity>
            <Image
                source={images.concert}
                resizeMode={"cover"}
                style={{
                    width: "45%",
                    borderRadius: 20,
                    height: "100%"
                }}
            />
            <View>
                <View style={{
                    paddingVertical: SIZES.small,
                    paddingHorizontal: SIZES.base,
                }}>
                    <Text style={{
                        color: COLORS.black,
                        fontWeight: "bold",
                        ...FONTS.h4
                    }}>Web Summit 2022</Text>
                </View>
                <View style={{
                    paddingHorizontal: SIZES.base,
                }}>
                    <Text style={{...FONTS.body5}}>The best new concert in Toronto are getting us through with wine and
                        tiki cocktails— all while
                        hopscotching through lockdown closures and reopening.</Text>
                </View>
                <View style={{
                    flexDirection: "row",
                    justifyContent: "space-around"
                }}>
                    <Text></Text>
                </View>
            </View>


        </View>
    )
}

export default CartItem;
