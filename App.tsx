import React, {useEffect, useState} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {EventRegister} from 'react-native-event-listeners';
import themeContext from "./theme/themeContext";
import theme from "./theme/theme";

import CustomDrawer from './stack/CustomNavigation';
import {MainLayout, SettingsScreen} from "./screen";
import {COLORS} from "./constants";
import {StatusBar} from "react-native";
import {Provider} from "react-redux";
import {store} from "./redux/store";
import RootStack from "./stack/RootStack";

const Stack = createStackNavigator();

const App = () => {
    const [mode, setMode] = useState<boolean>(false)

    useEffect(() => {
        let eventListener = EventRegister.addEventListener("changeTheme", (data) => {
            setMode(data)
        })
        return () => {
            if (typeof eventListener === "string") {
                EventRegister.removeEventListener(eventListener)
            }
        }
    }, [])

    return (
        <themeContext.Provider value={!mode ? theme.light : theme.dark}>
            <Provider store={store}>
                <StatusBar
                    backgroundColor={COLORS.primary}
                    barStyle={"dark-content"}
                />
                <NavigationContainer

                >
                    <RootStack />
                </NavigationContainer>
            </Provider>
        </themeContext.Provider>
    );
};

export default App;
